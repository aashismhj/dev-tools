# Services

## Devops
* [circleci](https://circleci.com/): CI/CD platform.

## Utils 
* [bit.dev](https://bit.dev/): An open source toolchain for the development of composable software.
* [quicktype](https://app.quicktype.io/): Create class, types of json string.
